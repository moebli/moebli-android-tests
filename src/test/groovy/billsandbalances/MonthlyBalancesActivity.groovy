package billsandbalances

import geb.mobile.android.AndroidBaseActivity

class MonthlyBalancesActivity extends AndroidBaseActivity {

    String getActivityName() {
        return "com.moebli.android.frontend.bill.MonthlyBalancesActivity"
    }

    static content = {
        balanceText { $("#balance_text") }
        nameText { $("#name_text") }
        createBillFab { $("#create_bill_fab") }
    }

    int balancesCount() {
        return balanceText.size();
    }
}
