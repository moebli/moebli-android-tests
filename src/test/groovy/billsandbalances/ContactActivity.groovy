package billsandbalances

import geb.mobile.android.AndroidBaseActivity

class ContactActivity extends AndroidBaseActivity {

    String getActivityName() {
        return "com.moebli.android.frontend.bill.ContactActivity"
    }

    static content = {
        billNameText { $("#bill_name_text") }
        billDateText { $("#bill_date_text") }
        amountText { $("#amount_text") }
    }

    int billsCount() {
        return billNameText.size();
    }
}
