package billsandbalances

import geb.mobile.android.AndroidBaseActivity

class CreateBillSuccessFragment extends AndroidBaseActivity {

    String getActivityName() {
        return "com.moebli.android.frontend.bill.CreateBillActivity"
    }

    static content =  {
        successMessage { $("#success_text") }
        billRows { $("#bill_row") }
    }
}
