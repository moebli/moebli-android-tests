package billsandbalances

import geb.mobile.GebMobileBaseSpec
import login.LoginActivity

class InitialBillsAndBalancesSpec extends GebMobileBaseSpec {

    def "contactBalances"() {
        given:
        waitFor(50) {
            at LoginActivity
        }
        when:
        login("dev-01@moebli.com", "welcome1875")
        then:
        at ContactBalancesActivity
        waitFor(10) {
            balancesCount() == 3
            $("#balance_text", 0).text() == "\$0.55"
            $("#balance_text", 1).text() == "\$0.20"
           // $("#balances-panel .balances > a", 2).$("span").text() == ""
           // $("#balances-panel .balances > a", 3).$("span").text() == ""
            $("#balance_text", 2).text() == "\$0.75"
        }
    }

    def "contactBills"() {
        when:
        $('#name_text', 1).click()
        then:
        at ContactActivity
        waitFor(10) {
            billsCount() == 2
        }
        when:
        driver.navigate().back()
        then:
        at ContactBalancesActivity
    }

    def "monthlyBalances"() {
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Monthly Balances')")
        when:
        $("text('Monthly Balances')").click()
        then:
        at MonthlyBalancesActivity
        waitFor(10) {
            balancesCount() == 1
        }
    }

    def "monthlyBills"() {
        when:
        $('#name_text', 0).click()
        then:
        at MonthlyBillsActivity
        waitFor(10) {
            billsCount() == 5
        }
    }

    def "itemBalances"() {
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at MonthlyBalancesActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Event Balances')")
        when:
        $("text('Event Balances')").click()
        then:
        at ItemBalancesActivity
        waitFor(10) {
            balancesCount() == 4
        }
    }

    def "itemBills"() {
        when:
        $('#name_text', 1).click()
        then:
        at ItemBillsActivity
        waitFor(10) {
            billsCount() == 2
        }
    }

    def "logout"() {
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ItemBalancesActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Logout')")
        when:
        $("text('Logout')").click()
        then:
        at LoginActivity
    }
}
