package billsandbalances

import geb.mobile.android.AndroidBaseActivity

class ItemBillsActivity extends AndroidBaseActivity {

    String getActivityName() {
        return "com.moebli.android.frontend.bill.ItemBillsActivity"
    }

    static content = {
        billNameText { $("#bill_name_text") }
    }

    int billsCount() {
        return billNameText.size()
    }
}
