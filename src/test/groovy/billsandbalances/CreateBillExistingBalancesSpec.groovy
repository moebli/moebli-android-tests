package billsandbalances

import geb.mobile.GebMobileBaseSpec
import login.LoginActivity

class CreateBillExistingBalancesSpec extends GebMobileBaseSpec {

    def createBillPage() {
        given:
        waitFor(50) {
            at LoginActivity
        }
        when:
        login("dev-01@moebli.com", "welcome1875")
        then:
        at ContactBalancesActivity
        waitFor(10) {
            balancesCount() == 3 && contacts() == 5
        }

        when:
        $('#name_text', 1).click()
        then:
        at ContactActivity
        waitFor(10) {
            billsCount() == 2
        }

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Monthly Balances')")
        when:
        $("text('Monthly Balances')").click()
        then:
        at MonthlyBalancesActivity
        waitFor(10) {
            balancesCount() == 1 || balancesCount() == 2
        }

        when:
        $('#name_text', 0).click()
        then:
        at MonthlyBillsActivity
        waitFor(10) {
            billsCount() == 5 || billsCount() == 4 //need to add 4 as when scroll comes it does not check for all bills in page, only displayed once.
        }

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at MonthlyBalancesActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Event Balances')")
        when:
        $("text('Event Balances')").click()
        then:
        at ItemBalancesActivity
        waitFor(10) {
            balancesCount() == 4
        }

        when:
        $('#name_text', 1).click()
        then:
        at ItemBillsActivity
        waitFor(10) {
            billsCount() == 2
        }

    }

    def createBill() {
        given:
        at ItemBillsActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ItemBalancesActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity
        when:
        createBillFab.click()
        then:
        at CreateBillActivity

        when:
        itemName = "Moss Beach Kayak"
        conn0 = "dev-06@moebli.com"
        then:
        waitFor(20) {
            conn0.text().contains("dev-06")
        }
        when:
        amount0 = "5.00"
        payToContact.click()
        sendButton.click()
        then:
        at CreateBillSuccessFragment
        waitFor(10) {
            successMessage.text().contains("Moss Beach Kayak")
            billRows.size() == 1
        }
    }

    def checkBalances() {
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity
        waitFor(10) {
            balancesCount() == 3
        }

        when:
        $('#name_text', 2).click()
        then:
        at ContactActivity
        waitFor(10) {
            billsCount() == 3
        }

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity

        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Monthly Balances')")
        when:
        $("text('Monthly Balances')").click()
        then:
        at MonthlyBalancesActivity
        waitFor(10) {
            balancesCount() == 2
        }

        when:
        $('#name_text', 0).click()
        then:
        at MonthlyBillsActivity
        waitFor(10) {
            billsCount() == 1
        }

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at MonthlyBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Event Balances')")
        when:
        $("text('Event Balances')").click()
        then:
        at ItemBalancesActivity
        waitFor(10) {
            balancesCount() == 5
        }

        when:
        $('#name_text', 4).click()
        then:
        at ItemBillsActivity
        waitFor(10) {
            billsCount() == 1
        }
    }
}
