package billsandbalances

import geb.mobile.android.AndroidBaseActivity

class ContactBalancesActivity extends AndroidBaseActivity {

    String getActivityName() {
        return "com.moebli.android.frontend.bill.ContactBalancesActivity"
    }

    static content = {
        balanceText { $("#balance_text") }
        nameText { $("#name_text") }
        createBillFab { $("#create_bill_fab") }
    }

    int balancesCount() {
        return balanceText.size()
    }

    int contacts() {
        return nameText.size()
    }
}
