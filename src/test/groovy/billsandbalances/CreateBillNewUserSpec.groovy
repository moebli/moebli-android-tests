package billsandbalances

import geb.mobile.GebMobileBaseSpec
import login.LoginActivity

class CreateBillNewUserSpec extends GebMobileBaseSpec {

    def emptyBalances() {
        given:
        waitFor(50) {
            at LoginActivity
        }
        when:
        login("dev-07@moebli.com", "welcome1875")
        then:
        at ContactBalancesActivity
        createBillFab.isDisplayed()

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Monthly Balances')")
        when:
        $("text('Monthly Balances')").click()
        then:
        at MonthlyBalancesActivity


        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Event Balances')")
        when:
        $("text('Event Balances')").click()
        then:
        at ItemBalancesActivity
    }

    def createBill() {
        given:
        at ItemBalancesActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity
        when:
        createBillFab.click()
        then:
        at CreateBillActivity

        when:
        itemName << "Stinson Beach"
        conn0 << "dev-05@moebli.com"
        amount0 << "5.00"
        sendButton.click()
        then:
        at CreateBillSuccessFragment
        successMessage.text().contains("Stinson")
        billRows.size() == 1
        //! blockedMessage.isDisplayed()
    }

    def checkBalances() {
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity
        waitFor(10) {
            balancesCount() == 1
        }

        when:
        $('#name_text', 0).click()
        then:
        at ContactActivity
        waitFor(10) {
            billsCount() == 1
        }

        when:
        driver.navigate().back()
        then:
        at ContactBalancesActivity

        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Monthly Balances')")
        when:
        $("text('Monthly Balances')").click()
        then:
        at MonthlyBalancesActivity
        waitFor(10) {
            balancesCount() == 1
        }

        when:
        $('#name_text', 0).click()
        then:
        at MonthlyBillsActivity
        waitFor(10) {
            billsCount() == 1
        }

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at MonthlyBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Event Balances')")
        when:
        $("text('Event Balances')").click()
        then:
        at ItemBalancesActivity
        waitFor(10) {
            balancesCount() == 1
        }

        when:
        $('#name_text', 0).click()
        then:
        at ItemBillsActivity
        waitFor(10) {
            billsCount() == 1
        }
    }

    def "logout"() {
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ItemBalancesActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Logout')")
        when:
        $("text('Logout')").click()
        then:
        at LoginActivity
    }

}
