package billsandbalances

import geb.mobile.android.AndroidBaseActivity

class CreateBillActivity extends AndroidBaseActivity {

    String getActivityName() {
        return "com.moebli.android.frontend.bill.CreateBillActivity"
    }

    static content = {
        itemName { $("#item_edit") }
        conn0 { $("#contact_1_edit") }
        amount0 { $("#amount_1_edit") }
        sendButton { $("#create_bill_button") }
        payToContact { $("#pay_to_conn_radio") }
    }
}
