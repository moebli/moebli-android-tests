package billsandbalances

import geb.mobile.GebMobileBaseSpec
import login.LoginActivity
import login.SplashScreenActivity

class UpdateBillToFromChangedSpec extends GebMobileBaseSpec {

    def updateBillSetup() {
        given:
        waitFor(50) {
            at LoginActivity
        }
        when:
        login("dev-01@moebli.com", "welcome1875")
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Monthly Balances')")
        when:
        $("text('Monthly Balances')").click()
        then:
        at MonthlyBalancesActivity

        when:
        $('#name_text', 0).click()
        then:
        at MonthlyBillsActivity


        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at MonthlyBalancesActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Event Balances')")
        when:
        $("text('Event Balances')").click()
        then:
        at ItemBalancesActivity

        when:
        $('#name_text', 2).click()
        then:
        at ItemBillsActivity

    }

    def updateBillFromContactBillsPage() {
        given:
        at ItemBillsActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ItemBalancesActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity

        when:
        $('#name_text', 2).click()
        then:
        at ContactActivity
        waitFor(5) {
            $('#amount_text', text: '$0.55').isDisplayed()
        }

        when:
        $('#bill_name_text', 1).click()
        then:
        at UpdateBillActivity
        waitFor(10) {
            amount.value() == '0.55'
        }

        /*when:TODO check how to do datePicker
        billDate.click()
        then:
        waitFor(10) {
            $(".android.widget.DatePicker").isDisplayed()
        }
        when:
        $("#date_picker_year").value("2016")
        $("#button1").click()
        then:
        at UpdateBillActivity*/
        when:
        payToContact.click()
        amount.value('.56')
        updateButton.click()
        then:
        at ContactActivity
        waitFor(10) {
            $('#amount_text', text: '$0.56').isDisplayed()
          //  balance.text() == '$0.36'
        }
    }

    def updateBillFromMonthlyBillsPage() {
        given:
        at ContactActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Monthly Balances')")
        when:
        $("text('Monthly Balances')").click()
        then:
        at MonthlyBalancesActivity


        when:
        $('#name_text', 0).click()
        then:
        at MonthlyBillsActivity
        waitFor(10) {
            $('#amount_text', text: '$0.56').isDisplayed()
        }

        when:
        $('#bill_name_text', 0).click()
        then:
        at UpdateBillActivity
        waitFor(10) {
            amount.value() == '0.56'
        }

        when:
        amount.value('.57')
        updateButton.click()
        then:
        at MonthlyBillsActivity
        waitFor(10) {
            $('#amount_text', text: '$0.57').isDisplayed()
            //  balance.text() == '$0.36'
        }
    }

    def updateBillFromItemBillsPage() {
        given:
        at MonthlyBillsActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at MonthlyBalancesActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Event Balances')")
        when:
        $("text('Event Balances')").click()
        then:
        at ItemBalancesActivity


        when:
        $('#name_text', 0).click()
        then:
        at ItemBillsActivity
        waitFor(10) {
            $('#amount_text', text: '$0.57').isDisplayed()
        }

        when:
        $('#bill_name_text', 0).click()
        then:
        at UpdateBillActivity
        waitFor(10) {
            amount.value() == '0.57'
        }

        when:
        amount.value('.55')
        updateButton.click()
        then:
        at ItemBillsActivity
        waitFor(10) {
            $('#amount_text', text: '$0.55').isDisplayed()
            //  balance.text() == '$0.36'
        }
    }

    def "logout"() {
        given:
        at ItemBillsActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ItemBalancesActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Logout')")
        when:
        $("text('Logout')").click()
        then:
        at LoginActivity
    }
}
