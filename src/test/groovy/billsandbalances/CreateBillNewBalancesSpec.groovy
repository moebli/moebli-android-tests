package billsandbalances

import geb.mobile.GebMobileBaseSpec
import login.LoginActivity

class CreateBillNewBalancesSpec extends GebMobileBaseSpec {

    def createBillPage() {
        given:
        waitFor(50) {
            at LoginActivity
        }
        when:
        login("dev-04@moebli.com", "welcome1875")
        then:
        at ContactBalancesActivity
        waitFor(10) {
            balancesCount() == 1
        }

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Monthly Balances')")
        when:
        $("text('Monthly Balances')").click()
        then:
        at MonthlyBalancesActivity
        waitFor(10) {
            balancesCount() == 1
        }


        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Event Balances')")
        when:
        $("text('Event Balances')").click()
        then:
        at ItemBalancesActivity
        waitFor(10) {
            balancesCount() == 1
        }

    }

    def createBill() {
        given:
        at ItemBalancesActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity
        when:
        createBillFab.click()
        then:
        at CreateBillActivity

        when:
        itemName << "Moss Beach Kayak"
        conn0 << "dev-06@moebli.com"
        amount0 << "5.00"
        payToContact.click()
        sendButton.click()
        then:
        at CreateBillSuccessFragment
        waitFor(10) {
            successMessage.text().contains("Moss Beach Kayak")
            billRows.size() == 1
        }
    }

    def checkBalances() {
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity
        waitFor(10) {
            balancesCount() == 2
        }

        when:
        $('#name_text', 2).click()
        then:
        at ContactActivity
        waitFor(10) {
            billsCount() == 1
        }

        when:
        driver.navigate().back()
        then:
        at ContactBalancesActivity

        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Monthly Balances')")
        when:
        $("text('Monthly Balances')").click()
        then:
        at MonthlyBalancesActivity
        waitFor(10) {
            balancesCount() == 2
        }

        when:
        $('#name_text', 0).click()
        then:
        at MonthlyBillsActivity
        waitFor(10) {
            billsCount() == 1
        }

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at MonthlyBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Event Balances')")
        when:
        $("text('Event Balances')").click()
        then:
        at ItemBalancesActivity
        waitFor(10) {
            balancesCount() == 2
        }

        when:
        $('#name_text', 0).click()
        then:
        at ItemBillsActivity
        waitFor(10) {
            billsCount() == 1
        }
    }

    def "logout"() {
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ItemBalancesActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Logout')")
        when:
        $("text('Logout')").click()
        then:
        at LoginActivity
    }
}
