package billsandbalances

import geb.mobile.android.AndroidBaseActivity

class UpdateBillActivity extends AndroidBaseActivity {

    String getActivityName() {
        return "com.moebli.android.frontend.bill.UpdateBillActivity"
    }

    static content = {
        amount { $("#amount_edit") }
        payToContact { $("#pay_to_contact_radio") }
        billDate { $("#bill_date_edit") }
        updateButton { $("#update_bill_button") }
    }
}
