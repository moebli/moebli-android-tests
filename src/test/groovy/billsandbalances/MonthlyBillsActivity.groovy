package billsandbalances

import geb.mobile.android.AndroidBaseActivity

class MonthlyBillsActivity extends AndroidBaseActivity {

    String getActivityName() {
        return "com.moebli.android.frontend.bill.MonthlyBillsActivity"
    }

    static content = {
        billNameText { $("#bill_name_text") }
    }

    int billsCount() {
        return billNameText.size()
    }
}
