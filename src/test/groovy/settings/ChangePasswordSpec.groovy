package settings

import billsandbalances.ContactBalancesActivity
import geb.mobile.GebMobileBaseSpec
import geb.waiting.WaitTimeoutException
import login.LoginActivity

class ChangePasswordSpec extends GebMobileBaseSpec {

    def "changePasswordPage"() {

        given:
        waitFor(50) {
            at LoginActivity
        }
        when:
        login("dev-01@moebli.com", "welcome1875")
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Profile Settings')")
        when:
        $("text('Profile Settings')").click()
        then:
        at AccountSettingsActivity
        when:
        changePassword.click()
        then:
        ChangePasswordActivity
    }

    def "changePasswordError"() {
        given:
        at ChangePasswordActivity
        when:
        oldPassword = "welcome1874"
        newPassword = "welcome1874"
        changeButton.click()
        then:
        at ChangePasswordActivity
        when:
        waitFor(10) {
            $("text('Password changed')")
        }
        then:
        thrown WaitTimeoutException
    }

    def "changePasswordSuccess"() {
        given:
        at ChangePasswordActivity
        when:
        oldPassword = "welcome1875"
        newPassword = "welcome1874"
        changeButton.click()
        then:
        at ChangePasswordActivity
        waitFor(10) {
            $("text('Password changed')")
        }

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at AccountSettingsActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Logout')")
        when:
        $("text('Logout')").click()
        then:
        at LoginActivity
    }

    def "loginWithNewPassword"() {

        given:
        at LoginActivity
        when:
        login("dev-01@moebli.com", "welcome1874")
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Profile Settings')")
        when:
        $("text('Profile Settings')").click()
        then:
        at AccountSettingsActivity
        when:
        changePassword.click()
        then:
        ChangePasswordActivity
    }

    def "changePasswordSuccess2"() {
        given:
        at ChangePasswordActivity
        when:
        oldPassword = "welcome1874"
        newPassword = "welcome1875"
        changeButton.click()
        then:
        at ChangePasswordActivity
        waitFor(10) {
            $("text('Password changed')")
        }
    }

    def "logout"() {
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at AccountSettingsActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Logout')")
        when:
        $("text('Logout')").click()
        then:
        at LoginActivity
    }
}
