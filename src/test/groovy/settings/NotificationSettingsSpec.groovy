package settings

import billsandbalances.ContactBalancesActivity
import geb.mobile.GebMobileBaseSpec
import login.LoginActivity

class NotificationSettingsSpec extends GebMobileBaseSpec {

    def "notificationSettingsActivity"() {
        given:
        waitFor(100) {
            at LoginActivity
        }
        when:
        login("dev-01@moebli.com", "welcome1875")
        then:
        waitFor(10) {
            at ContactBalancesActivity
        }

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Contact settings')")
        when:
        $("text('Contact settings')").click()
        then:
        true
    }
}
