package settings

import billsandbalances.ContactBalancesActivity
import geb.mobile.GebMobileBaseSpec
import geb.waiting.WaitTimeoutException
import login.LoginActivity

class ChangeAccountNameSpec extends GebMobileBaseSpec {

    def "changeAccountNamePage"() {
        given:
        waitFor(50) {
            at LoginActivity
        }
        when:
        login("dev-01@moebli.com", "welcome1875")
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Profile Settings')")
        when:
        $("text('Profile Settings')").click()
        then:
        at AccountSettingsActivity
        when:
        changeAccountName.click()
        then:
        at ChangeAccountNameActivity
    }

    def "changeAccountNameError"() {
        given:
        at ChangeAccountNameActivity
        when:
        accountName = "Seven<html>"
        changeButton.click()
        then:
        at ChangeAccountNameActivity
        when:
        waitFor(10) {
            $("text('Company name changed')")
        }
        then:
        thrown WaitTimeoutException
    }

    def "changeAccountNameSuccess"() {
        given:
        at ChangeAccountNameActivity
        when:
        accountName = "Seven-Eleven"
        changeButton.click()
        then:
        at ChangeAccountNameActivity
        waitFor(10) {
            $("text('Company name changed')")
        }
    }

    def "logout"() {
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at AccountSettingsActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Logout')")
        when:
        $("text('Logout')").click()
        then:
        at LoginActivity
    }
}
