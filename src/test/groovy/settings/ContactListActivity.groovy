package settings

import geb.mobile.android.AndroidBaseActivity

class ContactListActivity extends AndroidBaseActivity {

    String getActivityName() {
        return "com.moebli.android.frontend.contact.ContactListActivity"
    }

    static content = {
        contactName { $("#contact_name_text") }
        contactSettingMenu { $("#contact_context_menu") }
        invitedRows { $("#invitation_image") }
        blockedRows(wait:false, required:false) { $("#blocked_image") }
        showBlockedCheckBox { $("#show_blocked_checkbox") }
    }
}
