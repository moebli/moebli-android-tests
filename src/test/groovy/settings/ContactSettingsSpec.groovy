package settings

import billsandbalances.ContactBalancesActivity
import geb.mobile.GebMobileBaseSpec
import login.LoginActivity

class ContactSettingsSpec extends GebMobileBaseSpec {

    def "contactSettingsActivity" () {

        given:
        waitFor(50) {
            at LoginActivity
        }
        when:
        login("dev-01@moebli.com", "welcome1875")
        then:
        waitFor(10) {
            at ContactBalancesActivity
        }

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Contact settings')")
        when:
        $("text('Contact settings')").click()
        then:
        at ContactListActivity
        contactName.size() == 5
        invitedRows.size() == 1
        when:
        showBlockedCheckBox.click()
        then:
        at ContactListActivity
        blockedRows.size() == 1
    }

    def "reconnect" () {
        given:
        at ContactListActivity
        when:
        showBlockedCheckBox.value(true)
        then:
        contactName.size() == 6

        when:
        contactSettingMenu.getAt(1).click()
        then:
        waitFor(10) {
            $("text('Reconnect:')").isDisplayed()
        }

        when:
        $("text('Reconnect:')").click()
        then:
        at ContactListActivity
        blockedRows.present == false

    }

    def "blockEmail"() {
        given:
        at ContactListActivity
        when:
        contactSettingMenu.getAt(0).click()
        then:
        waitFor(10) {
            $("text('Block Email:')").isDisplayed()
        }
        when:
        $("text('Block Email:')").click()
        then:
        at ContactListActivity
    }

    def "disconnect"() {
        given:
        at ContactListActivity
        when:
        contactSettingMenu.getAt(2).click()
        then:
        waitFor(10) {
            $("text('Disconnect:')").isDisplayed()
        }
        when:
        $("text('Disconnect:')").click()
        then:
        at ContactListActivity
        blockedRows.present == true
    }

    def "logout"() {
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Logout')")
        when:
        $("text('Logout')").click()
        then:
        at LoginActivity
    }

    def "checkUpdates"() {
        given:
        at LoginActivity
        when:
        login("dev-01@moebli.com", "welcome1875")
        then:
        waitFor(10) {
            at ContactBalancesActivity
        }

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Contact settings')")
        when:
        $("text('Contact settings')").click()
        then:
        at ContactListActivity
        contactName.size() == 5
        invitedRows.size() == 1
        when:
        showBlockedCheckBox.click()
        then:
        at ContactListActivity
        blockedRows.size() == 1
    }

    def "reset"() {
        given:
        at ContactListActivity
        when:
        showBlockedCheckBox.value(true)
        then:
        contactName.size() == 6

        when:
        contactSettingMenu.getAt(2).click()
        then:
        waitFor(10) {
            $("text('Reconnect:')").isDisplayed()
        }

        when:
        $("text('Reconnect:')").click()
        then:
        at ContactListActivity
        blockedRows.present == false

        when:
        contactSettingMenu.getAt(0).click()
        then:
        waitFor(10) {
            $("text('Accept Email:')").isDisplayed()
        }
        when:
        $("text('Accept Email:')").click()
        then:
        at ContactListActivity

        when:
        contactSettingMenu.getAt(1).click()
        then:
        waitFor(10) {
            $("text('Disconnect:')").isDisplayed()
        }
        when:
        $("text('Disconnect:')").click()
        then:
        at ContactListActivity
        blockedRows.present == true

    }

    def "logout2"() {
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Logout')")
        when:
        $("text('Logout')").click()
        then:
        at LoginActivity
    }
}
