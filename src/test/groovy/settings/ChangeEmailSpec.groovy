package settings

import billsandbalances.ContactBalancesActivity
import geb.mobile.GebMobileBaseSpec
import geb.waiting.WaitTimeoutException
import login.LoginActivity

class ChangeEmailSpec extends GebMobileBaseSpec {
    def "changeEmailPage"() {
        given:
        waitFor(50) {
            at LoginActivity
        }
        when:
        login("dev-01@moebli.com", "welcome1875")
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Profile Settings')")
        when:
        $("text('Profile Settings')").click()
        then:
        at AccountSettingsActivity
        when:
        changeEmail.click()
        then:
        ChangeEmailActivity
    }

    def "changeEmailError"() {
        given:
        at ChangeEmailActivity
        when:
        email = "notanemail"
        changeButton.click()
        then:
        at ChangeEmailActivity
        when:
        waitFor(10) {
            $("text('A confirmation email has been sent to your new email address. Please click on the confirmation link to verify your email address.')")
        }
        then:
        thrown WaitTimeoutException
    }

    def "changeEmailSuccess"() {
        given:
        at ChangeEmailActivity
        when:
        email = "dev-00@moebli.com"
        changeButton.click()
        then:
        at ChangeEmailActivity
        waitFor(10) {
            $("text('A confirmation email has been sent to your new email address. Please click on the confirmation link to verify your email address.')")
        }

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at AccountSettingsActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Logout')")
        when:
        $("text('Logout')").click()
        then:
        at LoginActivity
    }

    def "loginWithNewEmail"() {
        given:
        at LoginActivity
        when:
        login("dev-00@moebli.com", "welcome1875")
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Profile Settings')")
        when:
        $("text('Profile Settings')").click()
        then:
        at AccountSettingsActivity
        when:
        changeEmail.click()
        then:
        ChangeEmailActivity
    }

    def "changeEmailSuccess2"() {
        given:
        at ChangeEmailActivity
        when:
        email = "dev-01@moebli.com"
        changeButton.click()
        then:
        at ChangeEmailActivity
        waitFor(10) {
            $("text('A confirmation email has been sent to your new email address. Please click on the confirmation link to verify your email address.')")
        }
    }

    def "logout"() {
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at AccountSettingsActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Logout')")
        when:
        $("text('Logout')").click()
        then:
        at LoginActivity
    }
}
