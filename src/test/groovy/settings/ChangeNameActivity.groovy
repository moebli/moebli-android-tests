package settings

import geb.mobile.android.AndroidBaseActivity

class ChangeNameActivity extends AndroidBaseActivity {

    String getActivityName() {
        return "com.moebli.android.frontend.user.ChangeNameActivity"
    }

    static content = {
        firstName { $("#firstname") }
        lastName { $("#lastname") }
        changeButton { $("#changeName") }
    }
}
