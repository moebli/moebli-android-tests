package settings

import geb.mobile.android.AndroidBaseActivity

class ChangeNotificationSettingActivity extends AndroidBaseActivity {

    String getActivityName() {
        return "com.moebli.android.frontend.user.ChangeNotificationSettingActivity"
    }

    static content = {

    }
}
