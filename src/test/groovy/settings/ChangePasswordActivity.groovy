package settings

import geb.mobile.android.AndroidBaseActivity

class ChangePasswordActivity extends AndroidBaseActivity {

    String getActivityName() {
        return "com.moebli.android.frontend.user.ChangePasswordActivity"
    }

    static content = {
        oldPassword { $("#password1") }
        newPassword { $("#password2") }
        changeButton { $("#changePassword") }
    }
}
