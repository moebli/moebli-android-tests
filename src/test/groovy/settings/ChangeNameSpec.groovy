package settings

import billsandbalances.ContactBalancesActivity
import geb.mobile.GebMobileBaseSpec
import geb.waiting.WaitTimeoutException
import login.LoginActivity

class ChangeNameSpec extends GebMobileBaseSpec {

    def "changeNamePage"() {

        given:
        waitFor(50) {
            at LoginActivity
        }
        when:
        login("dev-01@moebli.com", "welcome1875")
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Profile Settings')")
        when:
        $("text('Profile Settings')").click()
        then:
        at AccountSettingsActivity
        when:
        changeName.click()
        then:
        at ChangeNameActivity
    }

    def "changeNameError"() {
        given:
        at ChangeNameActivity
        when:
        firstName = ""
        lastName = ""
        changeButton.click()
        then:
        at ChangeNameActivity
        when:
        waitFor(10) {
            $("text('Name changed')")
        }
        then:
        thrown WaitTimeoutException
    }

    def "changeNameError2"() {
        given:
        at ChangeNameActivity
        when:
        firstName = "!24"
        lastName = "32@"
        changeButton.click()
        then:
        at ChangeNameActivity
        when:
        waitFor(10) {
            $("text('Name changed')")
        }
        then:
        thrown WaitTimeoutException
    }

    def "changeNameSuccess"() {
        given:
        at ChangeNameActivity
        when:
        firstName = "Jennifer"
        lastName = "Aniston"
        changeButton.click()
        then:
        at ChangeNameActivity
        waitFor(10) {
            $("text('Name changed')")
        }
    }

    def "logout"() {
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at AccountSettingsActivity
        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Logout')")
        when:
        $("text('Logout')").click()
        then:
        at LoginActivity
    }
}
