package settings

import geb.mobile.android.AndroidBaseActivity

class ChangeEmailActivity extends AndroidBaseActivity {

    String getActivityName() {
        return "com.moebli.android.frontend.user.ChangeEmailActivity"
    }

    static content = {
        email { $("#email") }
        changeButton { $("#button") }
    }
}
