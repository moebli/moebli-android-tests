package settings

import geb.mobile.android.AndroidBaseActivity

/**
 * Created by sushant on 14/09/15.
 */
class AccountSettingsActivity extends AndroidBaseActivity {

    String getActivityName() {
        return "com.moebli.android.frontend.user.AccountSettingsActivity"
    }

    static content = {
        changeEmail { $("#update_email_link") }
        changeName { $("#update_name_link") }
        changePassword { $("#update_password_link") }
        changeAccountName { $("#update_acocunt_name_link") }
        notificationSettings { $("#notification_settings_link") }
    }
}
