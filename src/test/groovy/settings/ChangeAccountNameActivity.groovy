package settings

import geb.mobile.android.AndroidBaseActivity

class ChangeAccountNameActivity extends AndroidBaseActivity {

    String getActivityName() {
        return "com.moebli.android.frontend.user.ChangeAccountNameActivity"
    }

    static content = {
        accountName { $("#accountName") }
        changeButton { $("#changeAccountName") }
    }
}
