package login

import geb.mobile.android.AndroidBaseActivity

class SplashScreenActivity extends AndroidBaseActivity {

    static at = {
        waitFor {
            $("text('Not Now:')").text() == "Not Now:"
        }
    }

    String getActivityName() {
        return "com.moebli.android.frontend.generic.SplashScreenActivity"
    }
    static content = {
        notNow { $("text('Not Now:')") }
    }
}
