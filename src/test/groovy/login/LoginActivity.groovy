package login

import geb.mobile.android.AndroidBaseActivity

class LoginActivity extends AndroidBaseActivity {

    String getActivityName() {
        return "com.moebli.android.frontend.user.LoginActivity"
    }

    static content = {
        phoneOrEmail { $("#phone_or_email_edit_text") }
        password { $("#password_edit_text") }
        submitButton { $("#button") }
    }

    void login(String phoneOrEmail, String password) {
        this.phoneOrEmail = phoneOrEmail
        this.password = password
        submitButton.click()
    }
}
