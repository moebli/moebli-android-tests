package login

import billsandbalances.ContactBalancesActivity
import geb.mobile.GebMobileBaseSpec
import geb.waiting.WaitTimeoutException
import spock.lang.Stepwise

@Stepwise
class LoginSpec extends GebMobileBaseSpec {

    def "missing required fields"() {
        given:
        waitFor(50) {
            at LoginActivity
        }
        when:
            login("","")
        then:
        assert true
        when:
        waitFor {
            $("#name_text").text() == "Christel Chung"
        }
        then:
        thrown WaitTimeoutException

    }

    def "login successful"() {
        given:
         at LoginActivity
        when:
        login("dev-01", "welcome1875")
        then:
        waitFor(60) {
            $("#name_text").text() == "Christel Chung"
        }
    }

    def "logout"() {
        given:
        at ContactBalancesActivity

        when:
        $(".android.widget.ImageButton", 0).click()
        then:
        assert $("text('Logout')")
        when:
        $("text('Logout')").click()
        then:
        at LoginActivity
    }
}
