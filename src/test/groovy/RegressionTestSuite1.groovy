import login.LoginSpec
import org.junit.runner.RunWith
import org.junit.runners.Suite
import settings.*

@RunWith(Suite.class)
@Suite.SuiteClasses([LoginSpec, ChangeNameSpec, ChangeEmailSpec, ChangePasswordSpec, ChangeAccountNameSpec, ContactSettingsSpec])
class RegressionTestSuite1 {
}
