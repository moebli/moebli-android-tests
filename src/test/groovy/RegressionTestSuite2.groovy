import billsandbalances.CreateBillNewBalancesSpec
import billsandbalances.CreateBillNewUserSpec
import billsandbalances.InitialBillsAndBalancesSpec
import billsandbalances.UpdateBillToFromChangedSpec
import org.junit.runner.RunWith
import org.junit.runners.Suite

/**
 * Created by sushant on 22/08/15.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses([InitialBillsAndBalancesSpec,CreateBillNewUserSpec,CreateBillNewBalancesSpec,UpdateBillToFromChangedSpec])
class RegressionTestSuite2 {
}
